#include <iostream>
#include "logger.h"
#include "config.h"
#include "scheduler.h"
#include <mutex>

// std::mutex g_mutex;

// A function to be scheduled that simply prints a greeting message
void printGreeting(const std::string& name) {
    // std::lock_guard<std::mutex> lock(g_mutex);
    console->info("Hello, {}!", name);
    // std::cout<<"Hello, "<<name<<"!"<<std::endl;
}

void showFifoScheduler() {
    // Create a scheduler with default parameters, This will use simple FIFO scheduling
    task::Scheduler scheduler;

        // Push several tasks to the scheduler
    scheduler.push(printGreeting, "Alice");
    scheduler.push(printGreeting, "Bob");
    scheduler.push(printGreeting, "Charlie");

    SPDLOG_INFO("Main thread is doing some work...\n");

    // Give some time for the scheduler to process the tasks
    std::this_thread::sleep_for(std::chrono::seconds(2));

    // Stop the scheduler and clean up
    scheduler.stop();
    SPDLOG_INFO("All tasks processed\n");
}

int main()
{
    initLogger();
    SPDLOG_INFO("{}: {}\n", config::project_name, config::project_version);

    showFifoScheduler();
    return 0;
}
