# WIP

# **Table of Contents**

1. **Introduction**
2. **Glossary**
3. **High Level Design**
    - Architectural Overview
    - Components
    - API
    - User Stories
4. **Development Operations**
	- Build System
	- Filesystem Layout
	- CI/CD
	- Packaging
	- Development Environment
	- Technology Stack and Frameworks
5. **Design Considerations**
	- Security
	- Performance
	- Error Handling
	- Scalability
	- Maintainability
6. **Development Strategy**
7. **Low-Level Design**
	- Component Diagrams
	- Low level Design Considerations
	- Class Interactions
	- Algorithms
8. **Revision History**


# Motivation
The motivation behind this project is to showcase my software engineer capabilities on this simple system. Here are some of the things you will see in this project:
1. **Showcasing Technical Proficiency**: Utilizing modern C++17, the project demonstrates expertise in writing high-value, generic code and object-oriented design. This aids in maintainability and testing while highlighting efficiency in build systems and a strong grasp of core programming principles.
2. **Project Management Excellence**: From formulating well-structured requirements in collaboration with stakeholders to executing a clear high-level and detailed design, the project exemplifies best practices in project management. Performance tracking, code quality assessment, documentation, observability, resource optimization, and efficient CI/CD pipelines are all integral to this process.
3. **Holistic Approach**: The project doesn't stop at code but encompasses all aspects of modern software development. Package management, testability, and alignment with company resources are all carefully considered, ensuring that the final product is not just a proof of concept but a robust and scalable solution.
4. **Professional Development**: Finally, this project reflects a commitment to continuous learning and professional growth. It stands as evidence of the ability to translate abstract business requirements into a tangible, working system, marrying technical prowess with a deep understanding of the broader business context.
---
---


# 1. **Introduction**
The modern software ecosystem is rife with complex tasks that must be managed, scheduled, and executed in an efficient and reliable manner. Managing these tasks, particularly in a periodic fashion, requires a well-architected system that can handle the intricacies of task scheduling, execution, error handling, and more.

This document outlines the design for a Periodic Task Scheduler System, aimed at facilitating the management and execution of periodic tasks. The system is built around three core components:

1. **Task**: Responsible for encapsulating the user-defined callable objects and tracking the state and metadata of each task.
2. **PeriodicTaskScheduler**: Acts as the central manager for handling all periodic tasks, ensuring their timely execution based on user-defined schedules.
3. **ThreadPool**: A low-level component tasked with the actual execution of tasks, utilizing the thread-pool library for efficiency.

The user interacts with the PeriodicTaskScheduler to register, modify, or remove tasks, with the underlying Task and ThreadPool components handling the specifics of execution and resource management.

This system is designed to be robust and scalable, accommodating a wide range of use cases. It is constructed with considerations for performance, security, error handling, and maintainability, leveraging modern C++17 features and a selection of well-regarded libraries and tools.

Whether used within a large-scale distributed system or a single-host application, the Periodic Task Scheduler System aims to simplify the complex world of task management, offering a streamlined interface for developers and systems alike.

# 2. **Glossary**
- Define terminology and acronyms specific to the system.

# 3. **High-Level Design**

The High-Level Design outlines the primary components of the Periodic Task Scheduler system and how they interact. The design emphasizes ease of use, with sensible defaults and minimal required configuration, while offering flexibility for more advanced users. The main components are:

## 3.1 **Architectural Overview**

The system consists of three core components:

- **Task**: A representation of the user-defined callable entity along with its parameters, encapsulated with relevant metadata like next scheduled execution time and cancellation status.
- **PeriodicTaskScheduler**: Manages all periodic tasks, handling task scheduling, execution, and cancellation.
- **ThreadPool**: Executes the tasks in parallel, using a configurable number of threads.

## 3.2 **Components**
### 3.2.1 **Task**

The `Task` component encapsulates the user's callable entity and the related parameters, wrapping them into an `std::function<void()>`. It maintains state information like the next scheduled execution time and whether it's canceled or not. The Task must inherit from the `IPeriodicTask` interface class. Users can create there own task classes and set the PeriodicTaskScheduler to use it. 

### 3.2.2 **PeriodicTaskScheduler**

The `PeriodicTaskScheduler` component is responsible for:

- **Creating Tasks**: Accepts any callable entity and an interval period as a floating point, wrapping them into a `Task` object.
- **Managing Tasks**: Sorts tasks in a priority queue, with tasks next in line for execution at the top.
- **Scheduling and Execution**: Contains an internal thread running in an infinite loop, checking and executing tasks when their time comes, utilizing a ThreadPool.
- **Task Control**: Allows user to cancel, control, and get statistics on task execution through a Task UID.
- **Task Cancellation**: Utilizes a map of UID to task references for efficient task cancellation and cleanup.
- **Managing the metadata and stats**: Manages the information with regards to task execution and offer the user the ability to extract this information. Also responsible for cleaning up resource including metadata and stats
- 

### 3.2.3 **ThreadPool**

The `ThreadPool` is a low-level implementation responsible for parallel execution of tasks. It utilizes [bshoshany's thread-pool library](https://github.com/bshoshany/thread-pool) or a similar C++17 thread pool implementation.

## 3.3 **API**

The API allows the user to create a `PeriodicTaskScheduler`, customize policies such as thread safety, create and manage periodic tasks, and query task statistics. Default values are provided for convenience.

#### 3.4 **User Stories**

- **Creating a Task**: A user can easily create a periodic task by passing a callable entity and an interval period.
- **Managing Tasks**: Users can control tasks, such as canceling them or querying their status, through a unique Task UID.
- **Customizing Behavior**: Advanced users can modify default behaviors and policies as needed.


# 4. **Development Operations**

- **Build System**: Specify the tools used, such as CMake, Conan, and the basic directory structure.
- **Filesystem Layout**: Detail the organization of files and folders.
- **CI/CD**: Explain the integration with GitLab CI/CD and how automated build and test cycles are handled.
- **Packaging**: Describe how the code will be packaged, versioned, and any containerization strategy.
	-  Versioning
	- Containerization 
	- Artifact Management
- **Development Environment**: Provide information on the development setup, including required tools and configurations. Discuss the use of tools like `spdlog` for logging and error handling and vscode.
- **Technology Stack and Frameworks**: Outline the technologies, frameworks, and libraries used in the project (e.g., logging, testing, benchmarking).
	- Development Environment (VSCode)
	- Logging (spdlog) 
	- Testing (Gtest) 
	- CI/CD (GitLab)
	- Benchmark (Google Benchmark)
	- Static Analysis (clong-tidy, cppcheck)
	- Formatters (clang-format)
	- Artifact Management (Conan, GitLab Artifactory, Docker)
	- Documentation (Doxygen)
	- ThreadPool (bshoshany-thread-pool)
	- C++ package management (Conan)
	- Image management (Docker)
	- Coverage tool (lcov)

# 5. **Design Considerations**
- **Security**: Detail how the design takes security concerns into account.
- **Performance**: Discuss performance goals and how they are addressed in the design. We should aim to have a clear understanding of the system latency, throughput and resource utilizations.
- **Error Handling**: Explain how errors will be managed, including logging and user feedback.
- **Scalability**: Describe how the design will accommodate growth in usage or complexity.
- **Maintainability**: Consider future changes and how the design will facilitate updates and enhancements.

# 6. **Development Strategy**
Describe the approach to development, including setting up the project skeleton, iterative development, testing, documentation, performance tuning, and CI/CD integration.

1. **Set up the Project Skeleton**
    - Create a GitLab repository - I will be using GitLab CICD 
    - Set up CMake for the project.
    - Basic directory structure: `include/`, `tests/`, `examples/`, `benchmarks/`, etc.
    - Setup conan
    - Setup logging and error handling. Consider using `spdlog`
1. **Iterative Development**
    - Start with the core functionality of registering, canceling, and executing timers.
    - Layer on additional requirements such as thread safety, STL compatibility, and resource management.
2. **Testing and Documentation**
    - Parallelly write unit tests as features are developed.
	    - Use GTest as testing framework
    - Document functions and classes as they're created.
	    - Use doxygen as documentation format
1. **Performance and Final Touches**
    - After core functionalities are implemented, focus on performance benchmarks.
    - Final touches like samples, tutorials, and more extensive documentation.
2. **CICD Integration**
    - Once the library is functionally complete, integrate with a CI/CD platform.
    - Automated build and test cycles.
    - Automatically publish documentation for new version

# 7. **Low-Level Design**
* **Low Level Design Considerations**
    - ** `PeriodicTaskScheduler` Object**
        - **Copy Semantics:** Copying is not supported to avoid complexity and unexpected behavior. This ensures that tasks are neither duplicated nor shared across different scheduler instances.
        - **Move Semantics:** Move semantics are supported, allowing ownership transfer without duplicating underlying resources. This enforces a one-owner principle.
        - **Uniqueness of Scheduler:** The design allows for flexibility, enabling users to create a single or multiple instances as needed. Example for singleton pattern is provided:        
        ```c++
        std::unique_ptr<PeriodicTaskScheduler> get_periodic_scheduler()
        {
            static auto scheduler = std::make_unique<PeriodicTaskScheduler>();
            return scheduler;
        }
        ```
        - **Resource Management and Usage:** By being move-only and not enforcing a single instance, the design guides users in proper utilization, providing centralized task management without limiting accessibility within the application.
	* **Data Structures**
        - The task manager will keep a reference to the tasks in a map of UID to task references. This will allow for efficient task cancellation and cleanup yet the actual tesk are stored in the priority_queue.
        - `std::unordered_map` for the UID to Task reference which will allow for efficient task cancellation and cleanup.
        - `std::priority_queue` for the actual tasks based on their next execution time. This will allow for the next task to be scheduled to be at the top of the queue always
	- **Concurrency**
	    - If opting for a thread-safe design, we will use a simple `std::mutex` for synchronization the priority queue.
	    - thread pool should be delegated with the execution of the tasks. [bshoshany's thread-pool library](https://github.com/bshoshany/thread-pool) will be used because of its simplicity, its lightweight yet offer configurability and it can be retrieved with Conan package management winch greatly reduces the dependencies complexity 
	* **Callbacks**
	    - Callbacks might be stored as `std::function<void()>` to allow flexibility in what can be registered.
	    - Consider lambda functions, function pointers, and functors.
	    - Consider allowing user to bind parameters 

## 7.1 **System Flow**

Detailed description of the various flows within the system, including task creation, execution, cancellation, etc.

### 7.1.1 **Task Creation Flow**

1. **User Input**: The user passes a callable entity and its required interval period as a floating point as well as the callable parameters.
2. **Callable Wrapping**: The `PeriodicTaskScheduler` wraps the callable and its parameters in a `Task` object.
3. **UID Generation**: A unique Task UID is generated and returned to the user for future task control and statistics retrieval.
4. **Next Execution Time**: The next scheduled execution time for the task is set.
5. **Task Queueing**: The newly created `Task` object is pushed into a priority queue within the `PeriodicTaskScheduler`.

### 7.1.2 **Task Execution Flow**

1. **Task Retrieval**: The `PeriodicTaskScheduler`'s main loop checks the top task from the queue which is always the next task scheduled for execution.
2. **Execution Time Check**: The system checks if it is time to execute the retrieved task. if so, the task is poped from the queue
3. **ThreadPool Execution**: If it's time, the task is sent to the `ThreadPool` for execution.
4. **Task Rescheduling**: After execution, the task's next execution time is reset and the task is re-queued.

### 7.1.3 **Task Cancellation Flow**

User can ask to cancel a task using the task UID. 
`PeriodicTaskScheduler` will fetch the task reference and will set it to be canceled and return control back to the user. The next time the task is retrived, the cancellation flag will be checked and the task will be cleared from `PeriodicTaskScheduler` data structures. The metadata of the task execution will be kept for a predefined period the user can control


## 7.2 **Component Diagrams**
Include UML or other diagrams to illustrate the structure of components.

## 7.3 **Class Interactions**
Explain how classes and objects will interact, possibly with sequence diagrams.

## 7.4 **Algorithms**
Detail any specific algorithms or procedures used in the system, possibly including pseudocode.

# 8. **Revision History**
- Record the changes to the document, including dates, authors, and reasons for changes.