# WIP

# Task Manager Requirements
## Motivation
The motivation behind this project is to showcase my software engineer capabilities on this simple system. Here are some of the things you will see in this project:
1. **Showcasing Technical Proficiency**: Utilizing modern C++17, the project demonstrates expertise in writing high-value, generic code and object-oriented design. This aids in maintainability and testing while highlighting efficiency in build systems and a strong grasp of core programming principles.
2. **Project Management Excellence**: From formulating well-structured requirements in collaboration with stakeholders to executing a clear high-level and detailed design, the project exemplifies best practices in project management. Performance tracking, code quality assessment, documentation, observability, resource optimization, and efficient CI/CD pipelines are all integral to this process.
3. **Holistic Approach**: The project doesn't stop at code but encompasses all aspects of modern software development. Package management, testability, and alignment with company resources are all carefully considered, ensuring that the final product is not just a proof of concept but a robust and scalable solution.
4. **Professional Development**: Finally, this project reflects a commitment to continuous learning and professional growth. It stands as evidence of the ability to translate abstract business requirements into a tangible, working system, marrying technical prowess with a deep understanding of the broader business context.

------
------

## **Introduction**

This document outlines the requirements for the `PeriodicTaskScheduler` library. The library's primary goal is to enable the registration and periodic execution of user-defined callbacks in C++. The requirements are meant to be understood by both technical and non-technical stakeholders and are divided into functional and non-functional categories.
## **Scope Statement**

The scope of the `PeriodicTaskScheduler` library development project is confined to showcasing expertise in modern C++ development, specifically using C++17. It aims to demonstrate proficiency in planning and executing complex tasks, resulting in a functional library that handles periodic task scheduling. Primary development will focus on Ubuntu, with potential expansion to Windows. The library's core functionalities include registration of periodic callbacks, thread safety, STL compatibility, robust error handling, and comprehensive documentation.

## **Glossary of Terms**
- **API (Application Programming Interface)**: A set of rules that allows different software entities to communicate with each other.
- **Callback**: A function or method that gets executed at a later time, usually in response to some event or trigger.
- **CI/CD (Continuous Integration/Continuous Deployment)**: Practices that automate the building, testing, and deployment processes in software development.
- **Containerization**: The encapsulation of an application and its dependencies into a 'container', allowing it to be run consistently across various computing environments.
- **GitLab**: A web-based Git repository manager with CI/CD tools.
- **Library**: A collection of pre-compiled code (functions, classes, etc.) that can be reused in various programs.
- **MIT License**: A permissive free software license originating at the Massachusetts Institute of Technology (MIT).
- **RAII (Resource Acquisition Is Initialization)**: A programming principle used in C++ where the management of resources (memory, file handles, etc.) is tied to object lifetime.
- **STL (Standard Template Library)**: A collection of template classes and functions in C++ that provide general-purpose classes and functions with templates to implement many popular and commonly used algorithms and data structures like vectors, lists, queues, and stacks.
- **Thread Safety**: The property of a program or portion of code that allows it to operate correctly in a multithreading environment, where multiple threads are executing concurrently.
- **UID (Unique Identifier)**: An identifier that is guaranteed to be unique among all identifiers used in a particular system or context.
- **Ubuntu**: An open-source operating system based on the Linux kernel.
- **Unit Testing**: A software testing method by which individual units of source code are tested to determine whether they are fit for use.

## **Functional Requirements**
### 1. **Core Functionality**
- **Registration of Periodic Callbacks**: The library must support the registration of periodic callbacks, where a callback is any callable entity with parameters.
- **Unique Identifiers**: Each registered timer callback must be assigned a unique identifier (UID).
- **Task Handling**: The system must handle ill-formed tasks, i.e., tasks with execution times longer than the user-defined interval, and failed tasks, providing appropriate feedback.
- **User Control**: The user must have control over tasks, such as registration, cancellation, editing execution periods, statistics, and expiration times.
- **Task Execution**: Execution of tasks must be done in designated threads with configurable resources, and ill-formed tasks should be appropriately handled.
- **Task Execution Period**: The task execution period resolution must be in milliseconds.
### 2. **Thread Safety**
- **Thread-safe Option**: Users must have an option to select if the `PeriodicTaskScheduler` is thread-safe.
- **Performance**: No performance penalty should be incurred for users who opt out of thread safety.
### 3. **STL Compatibility**
- **Integration with STL**: The library must be compatible with the Standard Template Library (STL).
### 4. **Library Design**
- **Integration and Versioning**: The library must be easily integrable with modern C++ projects and follow semantic versioning.
### 5. **Testing**
- **Unit Testing**: The codebase must be unit-testable, considering the non-deterministic nature of timers.
### 6. **Code Quality**
- **Standards and Coverage**: Regular checks with static analysis tools, coding format, test coverage reports, and adherence to the [Google C++ style guide](https://google.github.io/styleguide/cppguide.html).
### 7. **Deployment and Distribution**
- **Containerization and Management**: The library must be containerized and stored in private artifactory with various options for deployment.
### 8. **Resource Management**
- **RAII Principles**: Resource management must follow the RAII principles, with proper allocation and deallocation strategies.
### 9. **Documentation**
- **API and Code Documentation**: Clear and maintained documentation of the API and codebase.
### 10. **Error Handling and Logging**
- **Logging and Exception Handling**: The library must include logging capabilities and handle errors and exceptions deliberately.
### 11. **Examples**
- **Sample Projects**: Provide example projects or use-cases.
### 12. **Dependencies**
- **Separation of Dependencies**: Differentiate development dependencies from core functionality dependencies.

## **Non-Functional Requirements**
### 1. **Performance**
- **Latency, Overhead, and Scalability**: Optimized performance in various aspects such as registration, execution, and handling large numbers of timers.
### 2. **Usability**
- **Intuitive API and Documentation**: Ensure clarity and ease of use through design and comprehensive documentation.
### 3. **Reliability**
- **Accuracy and Robustness**: Maintain accurate timing and robust error handling.
### 4. **Maintainability**
- **Modularity, Readable Code, and Versioning**: Adhere to best practices for maintainable code and version management.
### 5. **Portability**
- **Platform and Compiler Compatibility**: Ensure usability across major platforms and compilers.
### 6. **Security**
- **Thread Safety and Memory Safety**: Safeguard against concurrency-related issues and memory-related vulnerabilities.
### 7. **Interoperability**
- **STL and Third-party Integration**: Ensure smooth integration with STL and third-party tools.
### 8. **Resource Efficiency**
- **Memory and CPU Utilization**: Efficient use of system resources.

## **Assumptions and Constraints**
### **Assumptions:**
- Development will utilize modern tools and practices, aligned with C++17 standards.
- Primary support will be for Ubuntu 20.04, with consideration for future expansion to Windows.
- The project serves as a personal showcase; thus, its design and implementation will be tailored to demonstrate specific skills and expertise.
- CI/CD will use GitLab to showcase my knowledge 
### **Constraints:**
- Time: The total effort allocated to this project will not exceed three days.
- Technology: The project must adhere to C++17, using modern build systems and package management tools.
- Platform: Initial development and support will focus solely on Ubuntu.

## **Compliance and Standards**
- **License**: The library will be released under the MIT license.
- **Coding Standards**: Custom coding standards will be applied throughout the project, with a fallback to the [Google C++ style guide](https://google.github.io/styleguide/cppguide.html) for any disputes or ambiguities.
## **Acceptance Criteria**
The `PeriodicTaskScheduler` library will be considered accepted when it meets the following criteria:
- Successfully implements all core functionalities as defined in the requirements.
- Adheres to the specified coding standards and complies with the MIT license.
- Passes all unit tests and meets the target of 85% code coverage.
- Successfully builds and runs on Ubuntu, demonstrating the intended behavior.
- Accompanied by comprehensive documentation, including API descriptions and usage examples.
- Delivered within the three-day time constraint.

## **Quality Assurance Procedures**
In addition to employing static analyzers, ensuring a minimum of 85% code coverage, and utilizing formatters for code quality, the following quality assurance procedures will be followed:
- **Testable Design**: All components must be designed with testability in mind, enabling thorough unit testing.
- **Code Review or Peer Review**: Consider mentioning a code review process in the Quality Assurance Procedures section to further enforce code quality and adherence to standards.
- **Continuous Integration**: Implement a CI pipeline to automate building, testing, and analysis tasks. I will use GItLab as my CI/CD pipeline and artifact management. 
- **Performance Monitoring**: Regular performance benchmarks to ensure that the library meets the defined performance goals.