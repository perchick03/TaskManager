# Get git sha and set GIT_SHA variable
execute_process(
   COMMAND git rev-parse HEAD
   WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
   OUTPUT_VARIABLE GIT_SHA
   OUTPUT_STRIP_TRAILING_WHITESPACE
)



# Auto generated conf file that includes project name, version and git sha
configure_file(
    ${CMAKE_SOURCE_DIR}/config_files/config.h.in
    ${CMAKE_BINARY_DIR}/config_files/config.h
    ESCAPE_QUOTES
)
