# Enable cache if available
find_program(CCACHE_FOUND ccache)
if(CCACHE_FOUND)
  message(STATUS "Using ccache for compiler caching, CCACHE_DIR=$ENV{CCACHE_DIR}")
  set_property(GLOBAL PROPERTY RULE_LAUNCH_COMPILE ccache)
else()
  message(STATUS "ccache not found, compiler caching disabled")
endif()