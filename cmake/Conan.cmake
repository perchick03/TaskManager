macro(run_conan)
    # Check if Conan is installed, and download it if not
    if(NOT EXISTS "${CMAKE_BINARY_DIR}/conan.cmake")
        message(STATUS "Downloading conan.cmake from https://github.com/conan-io/cmake-conan")
        file(DOWNLOAD "https://raw.githubusercontent.com/conan-io/cmake-conan/0.18.1/conan.cmake"
                        "${CMAKE_BINARY_DIR}/conan.cmake"
                        TLS_VERIFY ON)
    endif()

    # Include the downloaded Conan script
    include(${CMAKE_BINARY_DIR}/conan.cmake)

    # Automatically detect basic settings based on CMake configuration
    conan_cmake_autodetect(conan_settings)

    # Run Conan to install dependencies as defined in conanfile.txt
    # This will install the required packages, and generate necessary integration files
    # BUILD missing ensures that the required packages are built if they're not found
    conan_cmake_install(PATH_OR_REFERENCE ${CMAKE_SOURCE_DIR}
                        BUILD missing
                        REMOTE conancenter
                        SETTINGS ${conan_settings})


    # Include the generated file with all the variables and targets created by Conan
    # This helps in linking with the installed libraries
    include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)

    # Call this function to add global definitions and include directories
    # to your project, and to gather all the libraries to link with in ${CONAN_LIBS}
    conan_basic_setup()
endmacro()