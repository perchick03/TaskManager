
if(ENABLE_COVERAGE AND CMAKE_CXX_COMPILER_ID MATCHES "GNU|Clang")
    message("Enabling code coverage...")
    # Remove old gcda files
    # https://github.com/Tastyep/TaskManager/blob/master/cmake/CleanCoverage.cmake
    file(GLOB_RECURSE GCDA_FILES "${PROJECT_BINARY_DIR}/*.gcda")
    if(NOT GCDA_FILES STREQUAL "")
      file(REMOVE ${GCDA_FILES})
    endif()

    set(CMAKE_BUILD_TYPE Debug)
    set(COVERAGE_COMPILER_FLAGS "-g -O0 --coverage")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} --coverage -fprofile-arcs -ftest-coverage")
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${COVERAGE_COMPILER_FLAGS}")
endif()