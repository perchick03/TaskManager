
if(ENABLE_STATIC_ANALYSIS)
    message("Setting up static analysers")
    find_program(CLANGTIDY NAMES "clang-tidy-12")
    if(CLANGTIDY)
        message("Found Clang Tidy")
        # construct the clang-tidy command line
        set(CLANG_TIDY_OPTIONS
            ${CLANGTIDY}
            -extra-arg=-Wno-unknown-warning-option
            -extra-arg=-Wno-ignored-optimization-argument
            -extra-arg=-Wno-unused-command-line-argument
            -p)
        if(NOT "${CMAKE_CXX_STANDARD}" STREQUAL "")
            set(CLANG_TIDY_OPTIONS ${CLANG_TIDY_OPTIONS} -extra-arg=-std=c++${CMAKE_CXX_STANDARD})
        endif()

        message("Setting clang-tidy to ${CLANGTIDY}")
        set(CMAKE_CXX_CLANG_TIDY "${CLANG_TIDY_OPTIONS}")
    endif()


    find_program(CPPCHECK cppcheck)
    if(CPPCHECK)
        message("Found CPPCHECK")
        if(CMAKE_GENERATOR MATCHES ".*Visual Studio.*")
            set(CPPCHECK_TEMPLATE "vs")
        else()
            set(CPPCHECK_TEMPLATE "gcc")
        endif()

        set(CPPCHECK_OPTIONS
            --enable=warning,style,performance,portability,information,missingInclude
            --inline-suppr
            # We cannot act on a bug/missing feature of cppcheck
            --suppress=cppcheckError
            --suppress=internalAstError
            # if a file does not have an internalAstError, we get an unmatchedSuppression error
            --suppress=unmatchedSuppression
            # noisy and incorrect sometimes
            --suppress=passedByValue
            # ignores code that cppcheck thinks is invalid C++
            --suppress=syntaxError
            --suppress=preprocessorErrorDirective
            --inconclusive
            --suppress=*:*/.conan/*
            # warnings as errors
            # --error-exitcode=2
            )

        set(CMAKE_CXX_CPPCHECK ${CPPCHECK} --template=${CPPCHECK_TEMPLATE} ${CPPCHECK_OPTIONS})

        if(NOT "${CMAKE_CXX_STANDARD}" STREQUAL "")
            set(CMAKE_CXX_CPPCHECK ${CMAKE_CXX_CPPCHECK} --std=c++${CMAKE_CXX_STANDARD})
        endif()
    endif(CPPCHECK)
endif(ENABLE_STATIC_ANALYSIS)