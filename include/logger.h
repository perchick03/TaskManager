#pragma once

#include <string>
#include <memory>
#include "spdlog/spdlog.h"

constexpr std::string_view LOG_PATTERN = "[%Y-%m-%d][%t][%n][%^%l%$][%s:%!:%#]::%v";
void initLogger();

// TODO: This crappy logger don't want to set the file name and line number, have no idea why

// console logger
extern std::shared_ptr<spdlog::logger> console;
// extern std::shared_ptr<spdlog::logger> err_logger;


std::shared_ptr<spdlog::logger> init_logger(const std::string& name, const std::string& pattern = "");
