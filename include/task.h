#pragma once

#include "logger.h"
#include <chrono>
#include <functional>
#include <iostream>
#include <string>
#include <vector>

namespace task {

using Clock = std::chrono::steady_clock;
using TimePoint = Clock::time_point;
using Duration = std::chrono::duration<long long, std::milli>;
using TaskId = std::size_t;

class ITask
{
public:
	ITask() = default;
	virtual ~ITask() = default;

	virtual void operator()() = 0;
};

class Task : public ITask
{
public:
	explicit Task(std::function<void()> func, TaskId taskId)
		: m_func(std::move(func))
		, m_id(taskId)
	{ }

	void operator()() override
	{
		m_func();
	}

	[[nodiscard]] TaskId taskId() const
	{
		return m_id;
	}

	std::string toString() const
	{
		return std::to_string(m_id);
	}

	[[nodiscard]] bool operator==(const Task& other) const
	{
		return m_id == other.m_id;
	}

	[[nodiscard]] bool operator!=(const Task& other) const
	{
		return !(*this == other);
	}

	[[nodiscard]] bool operator<(const Task& other) const
	{
		return m_id < other.m_id;
	}

	void stop()
	{
		m_stop = true;
	}
	[[nodiscard]] bool isStopped() const
	{
		return m_stop;
	}
protected:
	bool m_stop{false};
	
private:
	TaskId m_id;
	std::function<void()> m_func;
	
};

class PeriodicTask : public Task
{
public:
	PeriodicTask(std::function<void()> func, Duration interval, TaskId taskId)
		: Task(std::move(func), taskId)
		, m_interval(interval)
		, m_next_execution_time{std::chrono::steady_clock::now() + interval}
	{ }

	[[nodiscard]] const Duration& interval() const
	{
		return m_interval;
	}
	[[nodiscard]] const TimePoint& nextExecutionTime() const
	{
		return m_next_execution_time;
	}
	void resetNextExecutionTime()
	{
		m_next_execution_time = Clock::now() + m_interval;
	}

	void operator()() override
	{
		if(!m_stop) { return; }
		TimePoint start, end;
		try
		{
			start = Clock::now();
			Task::operator()();
			end = Clock::now();
		}
		catch(const std::exception& e)
		{
			// TODO: add error handling
			SPDLOG_ERROR("Task {} throw exception cought from {}\n", toString(), e.what());
			m_stop = true;
		}

		SPDLOG_INFO("Task {} executed in {}ms", toString(), std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count());
		m_next_execution_time = end + m_interval;
	}


	[[nodiscard]] std::string toString() const
	{
		return Task::toString() + ", interval: " + std::to_string(m_interval.count()) + "ms";
	}

	friend std::ostream& operator<<(std::ostream& os, const PeriodicTask& task)
	{
		os << "PeriodicTask: " << task.toString();
		return os;
	}

private:
	Duration m_interval;
	TimePoint m_next_execution_time;
};

struct PeriodicTaskComparator
{
	/**
     * @brief Compare the next execution time of two tasks
     * 
     * @param lhs - PeriodicTask left hand side
     * @param rhs - PeriodicTask right hand side
     */
	bool operator()(const PeriodicTask& lhs, const PeriodicTask& rhs) const
	{
		return lhs.nextExecutionTime() > rhs.nextExecutionTime();
	}
};

} //namespace task