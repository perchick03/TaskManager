#include <atomic>
#include <memory>
#include <mutex>
#include <queue>
#include <thread>
#include <unordered_map>

#include "BS_thread_pool.hpp"
#include "task.h"

namespace task {

template <
	typename TaskT = Task,
	// By default, uses FIFO (queue) scheduling
	// TODO: set interface. TaskContainer must support push, emplace, front, pop and empty method
	typename TaskContainer = std::queue<std::shared_ptr<TaskT>>,
	// template <class, class> class TaskContainer = std::queue,
	// Must support push_task(TaskT task) method
	typename Executor = BS::thread_pool
>
class Scheduler
{
public:
	static_assert(std::is_base_of<ITask, TaskT>::value, "TaskT must implement ITask interface");

	explicit Scheduler(int maxWorkers = std::thread::hardware_concurrency())
		: m_executor(std::make_unique<Executor>(maxWorkers))
		, m_scheduler_thread(std::make_unique<std::thread>(&Scheduler::run, this))
	{
		assert(maxWorkers > 0 && "Max workers must be greater than 0");
	}

	~Scheduler()
	{
		stop();
	};

	// Disable copy semantics
	Scheduler(const Scheduler&) = delete;
	Scheduler& operator=(const Scheduler&) = delete;

	Scheduler(Scheduler&& other) noexcept
	{
		std::lock_guard<std::mutex> lock(other.m_mutex);
		m_executor = std::move(other.m_executor);
		m_tasks = std::move(other.m_tasks);
		m_uid_to_tasks = std::move(other.m_uid_to_tasks);
		m_scheduler_thread = std::move(other.m_scheduler_thread);
		m_stop.store(other.m_stop.load());
	}

	Scheduler& operator=(Scheduler&& other) noexcept
	{
		if(this != &other)
		{
			// Lock both mutexes without deadlock and adopt the ownership of the mutexes
			std::lock(m_mutex, other.m_mutex);
			std::lock_guard<std::mutex> lock1(m_mutex, std::adopt_lock);
			std::lock_guard<std::mutex> lock2(other.m_mutex, std::adopt_lock);

			m_executor = std::move(other.m_executor);
			m_tasks = std::move(other.m_tasks);
			m_uid_to_tasks = std::move(other.m_uid_to_tasks);
			m_scheduler_thread = std::move(other.m_scheduler_thread);
			// m_stop = std::move(other.m_stop);
			m_stop.store(other.m_stop.load());
		}
		return *this;
	}

	template <typename Func, typename... Args>
	TaskId push(Func&& func, Args&&... args)
	{
		const auto taskId = genUid();
		assert(m_uid_to_tasks.find(taskId) == m_uid_to_tasks.end() && "Task with this id already exists");

		auto task = std::make_shared<TaskT>(std::bind(std::forward<Func>(func), std::forward<Args>(args)...), taskId);
		{
			std::lock_guard<std::mutex> lock(m_mutex);
			m_uid_to_tasks[task->taskId()] = task;
			m_tasks.emplace(task);
		}
		console->debug("Task with id {} pushed", taskId);
		return taskId;
	}

	void cancelTask(TaskId taskId)
	{
		SPDLOG_DEBUG("Canceling task with id {}", taskId);
		
		std::lock_guard<std::mutex> guard(m_mutex);
		if(auto task_pair = m_uid_to_tasks.find(taskId); task_pair != m_uid_to_tasks.end())
		{
			// This will stop the task from running and will clean up the task from the queue next time the task is poped
			task_pair->second->stop();
			m_uid_to_tasks.erase(taskId);
			SPDLOG_INFO("Task with id {} canceled", taskId);
		}
	}

	void stop(){
		cleanupScheduler();
	}

protected:
	virtual std::shared_ptr<TaskT> getNextTask()
	{
		std::shared_ptr<TaskT> task{nullptr};

		std::lock_guard<std::mutex> lock(m_mutex);

		if(!m_tasks.empty())
		{
			task = m_tasks.front();
			m_tasks.pop();
		}
		return task;
	}

	virtual void run()
	{
		console->info("Scheduler thread started");
		
		while(!m_stop)
		{
			if(auto task = getNextTask())
			{
				auto task_wrapper = [&, t = task]() mutable {
					bool task_falied = false;
					if(!t->isStopped())
					{	
						try
						{
							(*t)();
						}
						catch(const std::exception& e)
						{
							task_falied = true;
							SPDLOG_ERROR("Task with id {} failed with exception: {}", t->taskId(), e.what());
						}
						catch(...)
						{
							task_falied = true;
							SPDLOG_ERROR("Task with id {} failed with unknown exception", t->taskId());
						}
					}

					std::lock_guard<std::mutex> lock(m_mutex);
					m_uid_to_tasks.erase(t->taskId());
					console->info("Task with id {} finished execution {}", t->taskId(), task_falied ? "with error" : "successfully");
				};

				m_executor->push_task(task_wrapper);
			}
			else
			{
				std::this_thread::sleep_for(std::chrono::milliseconds(100));
			}
		}
	}

	static TaskId genUid()
	{
		static std::atomic<TaskId> uid{0};
		return ++uid;
	}

	void cleanupScheduler()
	{
		SPDLOG_DEBUG("Cleaning up scheduler");
		// Gracefully stop all tasks
		for(auto& task_pair : m_uid_to_tasks)
		{
			task_pair.second->stop();
		}

		// atomically stop the scheduler thread and wait for it to finish
		m_stop = true;
		if(m_scheduler_thread && m_scheduler_thread->joinable())
		{
			m_scheduler_thread->join();
		}


		m_executor.reset();
		m_uid_to_tasks.clear();
		// m_tasks = TaskContainer();
	}

private:
	std::unique_ptr<Executor> m_executor;
	TaskContainer m_tasks;
	std::unordered_map<TaskId, std::shared_ptr<TaskT>> m_uid_to_tasks;
	std::mutex m_mutex;
	std::unique_ptr<std::thread> m_scheduler_thread;
	std::atomic<bool> m_stop{false};
};

} // namespace task