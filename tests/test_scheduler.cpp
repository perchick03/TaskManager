#include <gtest/gtest.h>
#include "scheduler.h"
#include "logger.h"

TEST(SchedulerTest, PushTasks) {
    task::Scheduler scheduler;

    auto taskId1 = scheduler.push([]() { std::cout << "Task 1\n"; });
    auto taskId2 = scheduler.push([]() { std::cout << "Task 2\n"; });

    EXPECT_NE(taskId1, taskId2) << "Task IDs should be different";
}

TEST(SchedulerTest, MoveConstructor) {
    task::Scheduler scheduler1;
    scheduler1.push([]() { std::cout << "Task 1\n"; });

    task::Scheduler scheduler(std::move(scheduler1));

    // You might need to expose some method to query the internal state for the actual checks
    // EXPECT_...(scheduler1, ...) << "After move, scheduler1 should be in a valid state";
    // EXPECT_...(scheduler2, ...) << "Scheduler2 should contain the moved data";
}

class TestScheduler : public task::Scheduler<> {
public:
    using task::Scheduler<>::getNextTask;
};

TEST(SchedulerTest, MoveAssignmentOperator) {
    TestScheduler scheduler1;
    scheduler1.push([]() { std::cout << "Task 1\n"; });

    TestScheduler scheduler2;
    scheduler2 = std::move(scheduler1);

    auto task2 = scheduler2.getNextTask();
    // You might need to expose some method to query the internal state for the actual checks
    // EXPECT_...(scheduler1, ...) << "After move, scheduler1 should be in a valid state";
    // EXPECT_...(scheduler2, ...) << "Scheduler2 should contain the moved data";
}



TEST(SchedulerTest, PushAndGetNextTask) {
    TestScheduler scheduler;

    auto taskId1 = scheduler.push([]() { /* some code */ });
    auto taskId2 = scheduler.push([]() { /* some code */ });

    auto task1 = scheduler.getNextTask();
    auto task2 = scheduler.getNextTask();

    EXPECT_EQ(taskId1, task1->taskId()) << "First task should have the first task ID";
    EXPECT_EQ(taskId2, task2->taskId()) << "Second task should have the second task ID";

    auto task3 = scheduler.getNextTask();
    EXPECT_EQ(nullptr, task3) << "No more tasks should be available";
}
