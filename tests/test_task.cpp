#include "gtest/gtest.h"
#include "task.h"

using namespace task;

TEST(TaskTest, TaskExecution) {
    bool wasExecuted = false;
    Task task([&wasExecuted](){ wasExecuted = true; }, 1);
    task();
    EXPECT_TRUE(wasExecuted);
}

TEST(TaskTest, TaskId) {
    const TaskId tastId = 5;
    const Task task([](){}, tastId);
    EXPECT_EQ(task.taskId(), tastId);
}

TEST(PeriodicTaskTest, Interval) {
    const Duration interval = std::chrono::milliseconds(100);
    const PeriodicTask periodicTask([](){}, interval, 1);
    EXPECT_EQ(periodicTask.interval(), interval);
}


TEST(PeriodicTaskTest, NextExecutionTime) {
    const Duration interval = std::chrono::milliseconds(100);
    const PeriodicTask periodicTask([](){}, interval, 1);
    const TimePoint expectedTime = Clock::now() + interval;
    EXPECT_NEAR(std::chrono::duration_cast<std::chrono::milliseconds>(periodicTask.nextExecutionTime() - expectedTime).count(), 0, 1);
}

TEST(PeriodicTaskComparatorTest, Comparison) {
    const Duration interval = std::chrono::milliseconds(100);
    const PeriodicTask task1([](){}, interval, 1);
    const PeriodicTask task2([](){}, interval * 2, 2);
    const PeriodicTaskComparator comparator;

    // Task1 has an earlier execution time than task2, so the comparator should return false.
    EXPECT_FALSE(comparator(task1, task2));
}


TEST(TaskTest, TaskEquality) {
    Task task1([](){}, 1);
    Task task2([](){}, 1);
    Task task3([](){}, 2);
    EXPECT_EQ(task1, task2);
    EXPECT_NE(task1, task3);
}

TEST(TaskTest, TaskLessThan) {
    Task task1([](){}, 1);
    Task task2([](){}, 2);
    EXPECT_LT(task1, task2);
}

TEST(TaskTest, TaskToString) {
    Task task([](){}, 1);
    EXPECT_EQ(task.toString(), "1");
}

TEST(PeriodicTaskTest, StopAndIsStopped) {
    PeriodicTask periodicTask([](){}, std::chrono::milliseconds(100), 1);
    EXPECT_FALSE(periodicTask.isStopped());
    periodicTask.stop();
    EXPECT_TRUE(periodicTask.isStopped());
}

TEST(PeriodicTaskTest, ResetNextExecutionTime) {
    Duration interval = std::chrono::milliseconds(100);
    PeriodicTask periodicTask([](){}, interval, 1);
    periodicTask.resetNextExecutionTime();
    TimePoint expectedTime = Clock::now() + interval;
    EXPECT_NEAR(std::chrono::duration_cast<std::chrono::milliseconds>(periodicTask.nextExecutionTime() - expectedTime).count(), 0, 1);
}

TEST(PeriodicTaskTest, ToString) {
    Duration interval = std::chrono::milliseconds(100);
    PeriodicTask periodicTask([](){}, interval, 1);
    EXPECT_EQ(periodicTask.toString(), "1, interval: 100ms");
}