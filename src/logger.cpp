#include "spdlog/sinks/stdout_color_sinks.h"
#include "spdlog/spdlog.h"

#include "logger.h"

#ifdef NDEBUG
#define DEBUG_LEVEL spdlog::level::info
#else
#define DEBUG_LEVEL spdlog::level::debug
#endif

void initLogger() {
    spdlog::set_pattern(std::string{LOG_PATTERN});
    
}

std::shared_ptr<spdlog::logger> init_logger(const std::string& name, const std::string& pattern) {

    // multi threaded console logger
    auto logger = spdlog::stdout_color_mt(name);    
    logger->set_level(DEBUG_LEVEL);
    logger->set_pattern(pattern.empty() ? std::string{LOG_PATTERN} : pattern);
    return logger;
}

std::shared_ptr<spdlog::logger> console = init_logger("console");
// std::shared_ptr<spdlog::logger> err_logger = init_logger("stderr"); 

